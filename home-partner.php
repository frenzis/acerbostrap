<div id="sliderpartner" class="fastlink col-sm-12">
    <h1 class="title compensate-bs">L'Acerbo è anche</h1>
    <?php
    if (function_exists('ot_get_option')) {
        $partnerText = ot_get_option('partner_text', array());
        if (!empty($partnerText)) {
            $doc = new DOMDocument();
            $doc->loadHTML('<meta http-equiv="content-type" content="text/html; charset=utf-8">' . $partnerText);
            $textareas = $doc->getElementsByTagName('p');
            foreach ($textareas as $textarea) {
                append_attr_to_element($textarea, 'class', 'lead');
            }
            echo $doc->saveHTML();
        }
    }
    ?>
    <div class="owl-partner owl-carousel">

        <?php
        if (function_exists('ot_get_option')) {
            $partners = ot_get_option('partner', array());
            if (!empty($partners)) {
                foreach ($partners as $partner) {
                    if (!empty($partner['image'])) {
                        $imgId = acerbo_get_image_id($partner['image']);
                        $imgSrc = wp_get_attachment_image_src($imgId, 'icon');
                        $imgSrcx2 = wp_get_attachment_image_src($imgId, 'iconx2');
                        if (!empty($partner['link'])) {
                            echo '<div class="item"><a href="' . $partner['link'] . '"><div class="ratio-container partner-container"><picture><!--[if IE 9]><video style="display: none;"><![endif]--><source media="(min-width: 768px)" data-srcset="' . $imgSrc[0] . '?width=' . $imgSrc[1] . '&height=' . $imgSrc[2] . '"><!--[if IE 9]></video><![endif]--><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-srcset="' . $imgSrcx2[0] . '?width=' . $imgSrcx2[1] . '&height=' . $imgSrcx2[2] . '" alt="'.trim(strip_tags(get_post_meta($imgId, '_wp_attachment_image_alt', true))).'" class="lazyload img-responsive"></picture></div></a></div>';
                        } else {
                            echo '<div class="item"><div class="ratio-container partner-container"><picture><!--[if IE 9]><video style="display: none;"><![endif]--><source media="(min-width: 768px)" data-srcset="' . $imgSrc[0] . '?width=' . $imgSrc[1] . '&height=' . $imgSrc[2] . '"><!--[if IE 9]></video><![endif]--><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-srcset="' . $imgSrcx2[0] . '?width=' . $imgSrcx2[1] . '&height=' . $imgSrcx2[2] . '" alt="'.trim(strip_tags(get_post_meta($imgId, '_wp_attachment_image_alt', true))).'" class="lazyload img-responsive"></picture></div></div>';
                        }
                    }
                }
            }
        }
        ?>
    </div>
</div>

<div class="clearfix"></div>