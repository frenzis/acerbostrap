<?php

/**
 * Copyright (c) 2014 Khang Minh <betterwp.net>
 * @license http://www.gnu.org/licenses/gpl.html GNU GENERAL PUBLIC LICENSE
 * @package BWP Google XML Sitemaps
 */
class BWP_GXS_MODULE_ALLEGATI extends BWP_GXS_MODULE {

    public function __construct() {
        // @since 1.3.0 this method is empty
    }

    protected function generate_data() {

        global $wpdb, $post;
// A standard custom query to fetch posts from database, sorted by their lastmod
// You can use any type of queries for your modules
        $latest_post_query = '
			SELECT * FROM ' . $wpdb->posts . "
				WHERE post_status = 'inherit' AND post_type = %s" . '
			ORDER BY post_modified DESC';
// Use $this->get_results instead of $wpdb->get_results, remember to escape your query
// using $wpdb->prepare or $wpdb->escape
        $latest_posts = $this->get_results($wpdb->prepare($latest_post_query, 'attachment'));
        
        

// This check helps you stop the cycling sooner
// It basically means if there is nothing to loop through anymore we return false so the cycling can stop.
        if (!isset($latest_posts) || 0 == sizeof($latest_posts))
            return false;

// Always init your $data
        $data = array();
        for ($i = 0; $i < sizeof($latest_posts); $i++) {
            $post = $latest_posts[$i];

            // Init your $data with the previous item's data. This makes sure no item is mal-formed.
            $data = $this->init_data($data);

            if ($using_permalinks && empty($post->post_name))
                $data['location'] = '';
            else
                $data['location'] = get_permalink();

            $data['lastmod'] = $this->get_lastmod($post);
            $data['freq'] = $this->cal_frequency($post);
            $data['priority'] = $this->cal_priority($post, $data['freq']);

            $this->data[] = $data;
        }

        unset($latest_posts);

        // always return true if we can get here,
        // otherwise we're stuck in a SQL cycling loop
        return true;
    }

}
