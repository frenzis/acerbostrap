/*!
 * jQuery Lifestream Plug-in
 * Show a stream of your online activity
 * @version   0.5.2
 * @author    Christian Vuerings et al.
 * @copyright Copyright 2014, Christian Vuerings - http://denbuzze.com
 * @license   https://github.com/christianv/jquery-lifestream/blob/master/LICENSE MIT
 */
/*global jQuery */
;(function( $ ){

  "use strict";

  /**
   * Initialize the lifestream plug-in
   * @param {Object} config Configuration object
   */
  $.fn.lifestream = function( config ) {

    // Make the plug-in chainable
    return this.each(function() {

      // The element where the lifestream is linked to
      var outputElement = $(this),

      // Extend the default settings with the values passed
      settings = jQuery.extend({
        // The name of the main lifestream class
        // We use this for the main ul class e.g. lifestream
        // and for the specific feeds e.g. lifestream-twitter
        classname: "lifestream",
        // Callback function which will be triggered when a feed is loaded
        feedloaded: null,
        // The amount of feed items you want to show
        limit: 10,
        // An array of feed items which you want to use
        list: []
      }, config),

      // The data object contains all the feed items
      data = {
        count: settings.list.length,
        items: []
      },

      // We use the item settings to pass the global settings variable to
      // every feed
      itemsettings = jQuery.extend( true, {}, settings ),

      /**
       * This method will be called every time a feed is loaded. This means
       * that several DOM changes will occur. We did this because otherwise it
       * takes to look before anything shows up.
       * We allow 1 request per feed - so 1 DOM change per feed
       * @private
       * @param {Array} inputdata an array containing all the feeditems for a
       * specific feed.
       */
      finished = function( inputdata ) {

        // Merge the feed items we have from other feeds, with the feeditems
        // from the new feed
        $.merge( data.items, inputdata );

        // Sort the feeditems by date - we want the most recent one first
        data.items.sort( function( a, b ) {
            return ( b.date - a.date );
        });

        var items = data.items,

            // We need to check whether the amount of current feed items is
            // smaller than the main limit. This parameter will be used in the
            // for loop
            length = ( items.length < settings.limit ) ?
              items.length :
              settings.limit,
            i = 0, item,

            // We create an unordered list which will create all the feed
            // items
            ul = $('<ul class="' + settings.classname + '"/>');

        // Run over all the feed items + add them as list items to the
        // unordered list
        for ( ; i < length; i++ ) {
          item = items[i];
          if ( item.html ) {
            $('<li class="'+ settings.classname + '-' +
               item.config.service + '">').data( "name", item.config.service )
                                          .data( "url", item.url || "#" )
                                          .data( "time", item.date )
                                          .append( item.html )
                                          .appendTo( ul );
          }
        }

        // Change the innerHTML with a list of all the feeditems in
        // chronological order
        outputElement.html( ul );

        // Trigger the feedloaded callback, if it is a function
        if ( $.isFunction( settings.feedloaded ) ) {
          settings.feedloaded();
        }

      },

      /**
       * Fire up all the feeds and pass them the right arugments.
       * @private
       */
      load = function() {

        var i = 0, j = settings.list.length;

        // We don't pass the list array to each feed  because this will create
        // a recursive JavaScript object
        delete itemsettings.list;

        // Run over all the items in the list
        for( ; i < j; i++ ) {

          var config = settings.list[i];

          // Check whether the feed exists, if the feed is a function and if a
          // user has been filled in
          if ( $.fn.lifestream.feeds[config.service] &&
               $.isFunction( $.fn.lifestream.feeds[config.service] ) &&
               config.user) {

            // You'll be able to get the global settings by using
            // config._settings in your feed
            config._settings = itemsettings;

            // Call the feed with a config object and finished callback
            $.fn.lifestream.feeds[config.service]( config, finished );
          }

        }

      };

      // Load the jQuery templates plug-in if it wasn't included in the page.
      // At then end we call the load method.
      if( !jQuery.tmpl ) {
        jQuery.getScript(
          '//ajax.aspnetcdn.com/ajax/jquery.templates/beta1/' +
          'jquery.tmpl.min.js',
          load);
      } else {
        load();
      }

    });

  };

  /**
   * Create a valid YQL URL by passing in a query
   * @param {String} query The query you want to convert into a valid yql url
   * @return {String} A valid YQL URL
   */
  $.fn.lifestream.createYqlUrl = function( query ) {
      return ( ('https:' === document.location.protocol ? 'https' : 'http') +
        '://query.yahooapis.com/v1/public/yql?q=__QUERY__' +
        '&env=' + 'store://datatables.org/alltableswithkeys&format=json')
        .replace( "__QUERY__" , encodeURIComponent( query ) );
  };

  /**
   * A big container which contains all available feeds
   */
  $.fn.lifestream.feeds = $.fn.lifestream.feeds || {};

  /**
   * Add compatible Object.keys support in older environments that do not natively support it
   * https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Object/keys#section_6
   */
  if(!Object.keys) {
    Object.keys = function(o){
      if (o !== Object(o)) {
        throw new TypeError('Object.keys called on non-object');
      }
      var ret=[],p;
      for(p in o) {
        if(Object.prototype.hasOwnProperty.call(o,p)) {
          ret.push(p);
        }
      }
      return ret;
    };
  }

}( jQuery ));


// FACEBOOK
(function($) {
$.fn.lifestream.feeds.facebook_page = function( config, callback ) {

  var template = $.extend({},
    {
      wall_post: 'post on wall <a href="${link}">${title}</a>'
    },
    config.template),

  /**
   * Parse the input from facebook
   */
  parseFBPage = function( input ) {
    var output = [], list, i = 0, j;

    if(input.query && input.query.count && input.query.count >0) {
      list = input.query.results.rss.channel.item;
      j = list.length;
      for( ; i<j; i++) {
        var item = list[i];
        if( $.trim( item.title ) ){
          output.push({
            date: new Date(item.pubDate),
            config: config,
            html: $.tmpl( template.wall_post, item )
          });
        }
      }
    }
    return output;
  };

  $.ajax({
    url: $.fn.lifestream.createYqlUrl('select * from xml where url="' +
      'www.facebook.com/feeds/page.php?id=' +
      config.user + '&format=rss20"'),
    dataType: 'jsonp',
    success: function( data ) {
      callback(parseFBPage(data));
    }
  });

  // Expose the template.
  // We use this to check which templates are available
  return {
    "template" : template
  };

};
})(jQuery);

//TWITTER

(function($) {
  "use strict";

  $.fn.lifestream.feeds.twitter = function(config, callback) {
    var template = $.extend({},
      {
        "posted": '{{html tweet}}'
      },
      config.template);

    /**
     * Add links to the twitter feed.
     * Hashes, @ and regular links are supported.
     * @private
     * @param {String} tweet A string of a tweet
     * @return {String} A linkified tweet
     */
    var linkify = function( tweet ) {

      var link = function( t ) {
        return t.replace(
          /([a-z]+:\/\/)([-A-Z0-9+&@#\/%?=~_|(\)!:,.;]*[-A-Z0-9+&@#\/%=~_|(\)])/ig,
          function( m, m1, m2 ) {
            return $("<a></a>").attr("href", m).text(
                ( ( m2.length > 35 ) ? m2.substr( 0, 34 ) + '...' : m2 )
            )[0].outerHTML;
          }
        );
      },
      at = function( t ) {
        return t.replace(
          /(^|[^\w]+)\@([a-zA-Z0-9_]{1,15})/g,
          function( m, m1, m2 ) {
            var elem = ($("<a></a>")
                     .attr("href", "https://twitter.com/" + m2)
                     .text("@" + m2))[0].outerHTML;
            return m1 + elem;
          }
        );
      },
      hash = function( t ) {
        return t.replace(
          /<a.*?<\/a>|(^|\r?\n|\r|\n|)(#|\$)([a-zA-Z0-9ÅåÄäÖöØøÆæÉéÈèÜüÊêÛûÎî_]+)(\r?\n|\r|\n||$)/g,
          function( m, m1, m2, m3, m4 ) {
            if (typeof m3 == "undefined") return m;
            var elem = "";
            if (m2 == "#") {
                elem = ($("<a></a>")
                        .attr("href",
                            "https://twitter.com/hashtag/" + m3 + "?src=hash")
                        .text("#" + m3))[0].outerHTML;
            } else if (m2 == "$") {
                elem = ($("<a></a>")
                        .attr("href",
                            "https://twitter.com/search?q=%24" + m3 + "&src=hash")
                        .text("$" + m3))[0].outerHTML;
            }
            return (m1 + elem + m4);
          }
        );
      };

      return hash(at(link(tweet)));

    },
    /**
     * Parse the input from twitter
     * @private
     * @param  {Object[]} items
     * @return {Object[]} Array of Twitter status messages.
     */
    parseTwitter = function(response) {
      var output = [];

      if (!response.tweets) {
        return output;
      }

      for(var i = 0; i < response.tweets.length; i++ ) {
        var status = response.tweets[i];

        output.push({
          "date": new Date(status.createdAt * 1000), // unix time
          "config": config,
          "html": $.tmpl( template.posted, {
            "tweet": linkify($('<div/>').html(status.text).text()),
            "complete_url": 'https://twitter.com/' + config.user +
              "/status/" + status.id,
			"data": new Date(status.createdAt * 1000)  
          } ),
          "url": 'https://twitter.com/' + config.user
        });
      }
      callback(output);
    };

    $.ajax({
      "url": 'https://twittery.herokuapp.com/' + config.user,
      "cache": false
    }).success(parseTwitter);

    // Expose the template.
    // We use this to check which templates are available
    return {
      "template" : template
    };

  };
})(jQuery);


//YOUTUBE

(function($) {
$.fn.lifestream.feeds.youtube = function( config, callback ) {

  var template = $.extend({},
    {
      uploaded: 'uploaded <a href="${video.player.default}" ' +
        'title="${video.description}">${video.title}</a>',
      favorited: 'favorited <a href="${video.player.default}" ' +
        'title="${video.description}">${video.title}</a>'
    },
    config.template),

  parseYoutube = function( input, activity ) {
    var output = [], i = 0, j, item, video, date, templateData;

    if(input.data && input.data.items) {
      j = input.data.items.length;
      for( ; i<j; i++) {
        item = input.data.items[i];

        switch (activity) {
          case 'favorited':
            video = item.video;
            date = item.created;
            templateData = item;
            break;
          case 'uploaded':
            video = item;
            date = video.uploaded;
            templateData = {video: video};
            break;
        }

        // Don't add unavailable items (private, rejected, failed)
        if (!video.player || !video.player['default']) {
          continue;
        }

        output.push({
          date: new Date(date),
          config: config,
          html: $.tmpl( template[activity], templateData )
        });
      }
    }

    return output;
  };

  $.ajax({
    url: "https://gdata.youtube.com/feeds/api/users/" + config.user +
      "/favorites?v=2&alt=jsonc",
    dataType: 'jsonp',
    success: function( data ) {
      callback(parseYoutube(data, 'favorited'));
    }
  });

  $.ajax({
    url: "https://gdata.youtube.com/feeds/api/users/" + config.user +
      "/uploads?v=2&alt=jsonc",
    dataType: 'jsonp',
    success: function( data ) {
      callback(parseYoutube(data, 'uploaded'));
    }
  });

  // Expose the template.
  // We use this to check which templates are available
  return {
    "template" : template
  };

};
})(jQuery);


//RSS

(function($) {
$.fn.lifestream.feeds.rss = function( config, callback ) {

  var template = $.extend({},
    {
      posted: 'posted <a href="${link}">${title}</a>'
    },
    config.template),

  /**
   * Get the link
   * @param  {Object} channel
   * @return {String}
   */
  getChannelUrl = function(channel){
    var i = 0, j = channel.link.length;

    for( ; i < j; i++) {
      var link = channel.link[i];
      if( typeof link === 'string' ) {
        return link;
      }
    }

    return '';
  },

  /**
   * Parse the input from rss feed
   */
  parseRSS = function( input ) {
    var output = [], list = [], i = 0, j = 0, url = '';
    if(input.query && input.query.count && input.query.count > 0) {
      list = input.query.results.rss.channel.item;
      j = list.length;
      url = getChannelUrl(input.query.results.rss.channel);

      for( ; i<j; i++) {
        var item = list[i];

        output.push({
          url: url,
          date: new Date( item.pubDate ),
          config: config,
          html: $.tmpl( template.posted, item )
        });
      }
    }
    return output;
  };

  $.ajax({
    url: $.fn.lifestream.createYqlUrl('select * from xml where url="' +
      config.user + '"'),
    dataType: 'jsonp',
    success: function( data ) {
      callback(parseRSS(data));
    }
  });

  // Expose the template.
  // We use this to check which templates are available
  return {
    "template" : template
  };

};
})(jQuery);