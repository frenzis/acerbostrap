(function ($) {

    var cal = $("#calendar");
    if (cal.length) {
        "use strict";

        var options = {
            events_source: '/wp-admin/admin-ajax.php?action=my_ajax_getevent',
            view: 'month',
            tmpl_path: "/wp-content/themes/acerbostrap/calendar/tmpls/",
            language: 'it-IT',
            tmpl_cache: true,
            day: 'now',
            onAfterEventsLoad: function (events) {
                if (!events) {
                    console.log('No event');
                    return;
                }
                var list = $('#eventlist');
                list.html('');

                $.each(events, function (key, val) {
                    $(document.createElement('li'))
                        .html('<a href="' + val.url + '">' + val.title + '</a>')
                        .appendTo(list);
                });
            },
            onAfterViewLoad: function (view) {
                $('.page-header h3').text(this.getTitle());
                $('.btn-group button').removeClass('active');
                $('button[data-calendar-view="' + view + '"]').addClass('active');
            },
            classes: {
                months: {
                    general: 'label'
                }
            }
        };

        var calendar = cal.calendar(options);

        $('.btn-group button[data-calendar-nav]').each(function () {
            var $this = $(this);
            $this.click(function () {
                calendar.navigate($this.data('calendar-nav'));
            });
        });

        $('.btn-group button[data-calendar-view]').each(function () {
            var $this = $(this);
            $this.click(function () {
                calendar.view($this.data('calendar-view'));
            });
        });

        $('#first_day').change(function () {
            var value = $(this).val();
            value = value.length ? parseInt(value) : null;
            calendar.setOptions({
                first_day: value
            });
            calendar.view();
        });

        $('#language').change(function () {
            calendar.setLanguage($(this).val());
            calendar.view();
        });

        $('#events-in-modal').change(function () {
            var val = $(this).is(':checked') ? $(this).val() : null;
            calendar.setOptions({
                modal: val
            });
        });
        $('#events-modal .modal-header, #events-modal .modal-footer').click(function (e) {
            //e.preventDefault();
            //e.stopPropagation();
        });

    }
}(jQuery));