/*
 * Funzioni per creazione e distruzione dell'accordion dello slider
 */
var ie8 = navigator.userAgent.match(/Trident\/4\./) ? true : false;

function runZaccordion(media, slideWidth, width) {

    if (media.length > 0) {
        media.zAccordion({
            height: 300,
            speed: 350,
            timeout: 2E3,
            startingSlide: 2,
            slideWidth: slideWidth,
            width: width,
            trigger: "click",
            auto: false,
            slideClass: "slider-media",
            animationStart: function () {
                media.find("div.slider-media-open div").css("display", "none");
                media.find("div.slider-media-previous div").css("display", "none");
            },
            animationComplete: function () {
                media.find("div.slider-media div").fadeIn(600);
            },
            buildComplete: function () {
                jQuery('<style type="text/css">#featured .slider-media-closed .featured-text { width: ' + ((width - slideWidth) / 5) + 'px; }</style>').appendTo(jQuery('head'));
                jQuery('#featured-inner').fadeIn(500);
                console.log('zAccordion built');
            }
        });

        var slideOpen = 0;
        jQuery("#featured .slider-media").hoverIntent({
            over: function () {
                slideIndex = jQuery(this).index();
                if (slideOpen != slideIndex) {
                    media.zAccordion("trigger", slideIndex);
                    slideOpen = slideIndex;
                    return false;
                }
            },
            out: jQuery.noop,
            interval: 150
        });
    }
}

function destroyZaccordion(media) {
    if (media.length > 0) {
        media.zAccordion('destroy', {
            removeStyleAttr: true,
            removeClasses: true,
            destroyComplete: {
                afterDestroy: function () {
                    try {
                        console.log('zAccordion destroyed');
                    } catch (e) {
                    }
                }
            }
        });
    }
}

jQuery(document).ready(function ($) {
    /*
     * EnquireJS
     */
    if (ie8 === false) {
        enquire.register("screen and (max-width: 991px)", {
            match: function () {
                destroyZaccordion($('#featured'));
            }

        })
            .register("screen and (min-width: 992px) and (max-width: 1199px)", {
                match: function () {
                    destroyZaccordion($('#featured'));
                    runZaccordion($('#featured'), 460, 968);
                }

            })
            .register("screen and (min-width: 1200px)", {
                match: function () {
                    destroyZaccordion($('#featured'));
                    runZaccordion($('#featured'), 460, 1168);
                }

            });
    } else {
        //runZaccordion($('#featured'), 540, 968);
    }
});