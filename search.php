<?php
$results = ($wp_query->found_posts == 1) ? 'C\'è un unico risultato' : 'Ci sono '.$wp_query->found_posts.' risultati';
?>

<?php get_header(); ?>


    <div id="content" class="col-md-8 bd-right">
        <h1 class="title compensate-bs">Hai cercato  "<?php the_search_query(); ?>". <?php echo $results; ?>. </h1>
        <div class="blocchetto">
            <?php
            if (have_posts()) :
                get_search_form();
                while (have_posts()): the_post();
                    get_template_part('acerbo', 'loop');
                endwhile;
            else :
                ?>              
                <div id="post-0" class="post not-found"> 
                    <h1 class="title compensate-bs"></h1>
                    <div class="post-bodycopy">       
                        <p>Prova a verificare la correttezza della ricerca o ad usare parole pi&ugrave; generiche</p>  
                        <?php get_search_form(); ?>
                    </div>           
                </div>      
            <?php endif; ?>
        </div>   
        </div>
        <div id="widgetarea-one" class="col-md-4">
<h1 class="title compensate-bs" style="margin-bottom: 25px"><i class="fa fa-list"></i>&nbsp;Sezioni</h1>
            <?php dynamic_sidebar('sidebar-1'); ?>   
        </div>   
    <?php get_footer(); ?>  