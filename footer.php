</div> <!-- main row -->

</div> <!-- container -->
<div id="footer-bg">
    <div id="footer" class="lw">
        <p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?> Pescara</p>
        <p><a href="//www.iubenda.com/privacy-policy/465191" class="iubenda-black iubenda-embed" title="Privacy Policy">Privacy</a></p>
        <p>
            <a href="<?php echo get_permalink(106); ?>" title="Privacy Policy">Contatti</a> |
            <a href="http://www.gazzettaamministrativa.it/opencms/opencms/_gazzetta_amministrativa/amministrazione_trasparente/_abruzzo/_istituto_tecnico_statale_commerciale_per_geometri_e_per_il_turismo_tito_acerbo_di_pescara">Ammministrazione Trasparente</a> |
            <a href="http://www.istitutotecnicoacerbope.gov.it/avcp/indicegare.xml">AVCP</a> |
            <a href="<?php echo get_permalink(1790); ?>">Note legali</a>

        </p>
        <p><strong>Per i contenuti precedenti al gennaio 2015 naviga sul <a href="http://win.istitutotecnicoacerbope.gov.it/">Vecchio sito</a></strong></p>
        <script type="text/javascript">(function(w, d) {
                var loader = function() {
                    var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0];
                    s.src = "//cdn.iubenda.com/iubenda.js";
                    tag.parentNode.insertBefore(s, tag);
                };
                if (w.addEventListener) {
                    w.addEventListener("load", loader, false);
                } else if (w.attachEvent) {
                    w.attachEvent("onload", loader);
                } else {
                    w.onload = loader;
                }
            })(window, document);</script>
        <p><small>Sito realizzato da <a href="http://www.caccavella.com" target="_blank">Francesco Caccavella</a>. Contenuti a cura di Marco Castiglione. Aggiornamenti a cura di Raffaele Odorisio.</small></p>
    </div>
</div>
</div> <!-- mb Cont -->
<?php get_template_part('acerbo', 'navigation'); ?>
</div> <!-- mb All -->
<?php
wp_footer();
global $developer;
if ($developer != 'yes') {
    include_once (get_template_directory() . '/build/critical/critical-js.php');
}
?>
<!--[if lt IE 9]>
<script src="/wp-content/themes/acerbostrap/build/js/footerie8.min.js"></script>

<?php
if (is_front_page()) {
    echo '<script src="/wp-content/themes/acerbostrap/build/js/home.min.js"></script>'
    . '<script src="/wp-content/themes/acerbostrap/build/js/zaccordion.min.js"></script>'
    . '<script src="/wp-content/themes/acerbostrap/build/js/zaccordion-app.min.js"></script>'
    . '<script src="/wp-content/themes/acerbostrap/build/js/owl-app.min.js"></script>';
}
if (is_page('aggiornamenti')) {
    echo '<script src="/wp-content/themes/acerbostrap/build/js/calendar.min.js"></script>';
}
?>
<script src="/wp-content/themes/acerbostrap/build/js/ajax-app.min.js"></script>
<script src="/wp-content/themes/acerbostrap/build/js/run-app.min.js"></script>
<![endif] -->
</body>
</html>