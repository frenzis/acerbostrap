<?php

// Raccoglie metadati dalle pagine WP in un array $dataLayer (cfr. http://goo.gl/Nmo1I)
// Da includere nell'head o comunque prima del tag di Google Tag Manager
global $current_user, $wp_query;
$this_id = get_the_ID();
$dataLayer = array();

if (is_multisite()) {
    global $blog_id;
    $current_blog_details = get_blog_details(array(
        'blog_id' => $blog_id
    ));
    $dataLayer["siteName"] = $current_blog_details->blogname;
} else {
    $dataLayer["siteName"] = get_bloginfo('name', 'display');
}
if (is_front_page()) {
    $dataLayer["pagePostType"] = "Homepage";
}

if (is_user_logged_in()) {
    $dataLayer["visitorLoginState"] = "logged-in";
    get_currentuserinfo();
    $dataLayer["visitorType"] = ($current_user->roles[0] == NULL ? "visitor-logged-out" : $current_user->roles[0]);
} else {
    $dataLayer["visitorLoginState"] = "logged-out";
}
if (is_singular() && !is_front_page()) {
    global $post;
    $dataLayer["pagePostID"] = $this_id;
    $dataLayer["pagePostType"] = get_post_type();
    $taxonomies = get_taxonomies('', 'names');
    foreach ($taxonomies as $taxonomy) {
        $terms = get_the_terms($this_id, $taxonomy);
        if (!empty($terms)) {
            $val = 0;
            foreach ($terms as $term) {
                $taxonomy = ucfirst($taxonomy);
                $dataLayer["page{$taxonomy}_{$val}"] = $term->name;
                $val++;
            }
            unset($val);
        }
    }


    $pattern = get_shortcode_regex();
    if (preg_match_all('/' . $pattern . '/s', $post->post_content, $matches, PREG_SET_ORDER)) {
        $val = 0;
        foreach ($matches as $value) {
            $dataLayer["pageShortcode_{$val}"] = $value[2];
            $val++;
        }
        unset($val);
    }

    if (current_user_can('activate_plugins')) {
        $post_customs = get_post_meta($this_id);
        if ($post_customs) {
            foreach ($post_customs as $key => $post_custom) {
                $single = get_post_meta($this_id, $key, true);
                if (is_array($single)) {
                    foreach ($single as $key => $value) {
                        $dataLayer["pageCustom_{$key}"] = $value;
                    }
                } else {
                    $dataLayer["pageCustom_{$key}"] = $single;
                }
            }
        }
    }
    $dataLayer["pagePostWordCount"] = str_word_count(strip_tags(get_post_field('post_content', $this_id)));
    $dataLayer["pagePostAuthor"] = get_the_author_meta('display_name', get_post_field('post_author', $this_id, 'raw'));
    $dataLayer["pagePostTitle"] = get_post_field('post_title', $this_id, 'raw');
    $dataLayer["pagePostDate"] = get_the_date('/Y/m/d/W/H/');
    $dataLayer["pagePostDateYear"] = get_the_date("Y");
    $dataLayer["pagePostDateMonth"] = get_the_date("m");
    $dataLayer["pagePostDateDay"] = get_the_date("d");
    $dataLayer["pagePostDateHour"] = get_the_date("H");
}
if (is_archive() || is_post_type_archive()) {
    if (is_category()) {
        $dataLayer["pagePostType"] = "Archivio Categoria";
    } elseif (is_tag()) {
        $dataLayer["pagePostType"] = "Archivio Tag";
    } elseif (is_tax()) {
        $dataLayer["pagePostType"] = "Archivio Tassonomia";
    } elseif (is_author()) {
        $dataLayer["pagePostType"] = "Archivio Autore";
    } elseif (is_year()) {
        $dataLayer["pagePostType"] = "Archivio Anno";
        $dataLayer["pagePostDateYear"] = get_the_date("Y");
    } elseif (is_month()) {
        $dataLayer["pagePostType"] = "Archivio Mese";
        $dataLayer["pagePostDateYear"] = get_the_date("Y");
        $dataLayer["pagePostDateMonth"] = get_the_date("m");
    } elseif (is_day()) {
        $dataLayer["pagePostType"] = "Archivio giorno";
        $dataLayer["pagePostDate"] = get_the_date();
        $dataLayer["pagePostDateYear"] = get_the_date("Y");
        $dataLayer["pagePostDateMonth"] = get_the_date("m");
        $dataLayer["pagePostDateDay"] = get_the_date("d");
    } elseif (is_time()) {
        $dataLayer["pagePostType"] = "Archivio ora";
    } elseif (is_date()) {
        $dataLayer["pagePostType"] = "Archivio data";
        $dataLayer["pagePostDate"] = get_the_date();
        $dataLayer["pagePostDateYear"] = get_the_date("Y");
        $dataLayer["pagePostDateMonth"] = get_the_date("m");
        $dataLayer["pagePostDateDay"] = get_the_date("d");
    }
    if ((is_tax() || is_category() || is_tag())) {
        $taxonomy = ucfirst(get_queried_object()->taxonomy);
        $name = get_queried_object()->name;
        $dataLayer["page{$taxonomy}"] = $name;
    }
}

echo '<script>';
printf("dataLayer = [%s]; \r\n", json_encode($dataLayer));
echo '</script>';
