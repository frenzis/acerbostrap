<div id="main-menu" class="cf menu_container dotted fulled maxistop">
    <?php
    wp_nav_menu(array(
        'container' => 'nav',
        'container_class' => 'menu-sticky menu-wrapper col12',
        'container_id' => 'menu1-wrapper',
        'menu_id' => 'menu1',
        'menu_class' => 'cf menu',
        'theme_location' => 'menu1',
        'walker' => new Child_Wrap()
    ));
    ?>
    <div class="dropdown_container" style="height: 80px; display: none"></div>
</div>