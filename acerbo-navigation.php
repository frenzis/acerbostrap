<div id="mbNav" data-navigation-handle=".mobile_handle" data-navigation-content="#mbCont"
     class="visible-xs-block mobile_navigation shifter-navigation ">
	<?php
	if ( ! is_search() ) {
		get_search_form();
	}
	?>
    <ul id="menu-menu-circolari" class="slider-menu list-group">
        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-circolari dropdown">
            <a title="Circolari" href="#circolari" data-toggle="collapse" data-parent="#menu-menu-circolari"
               aria-expanded="false" aria-controls="circolari">Ultime pubblicazioni <span class="caret"></span></a>
            <ul id="circolari" role="menu" class="collapse nav-collapse">
				<?php
				$args    = array(
					'posts_per_page' => 10,
					'post_type'      => array( 'circolare', 'post', 'avviso' )
				);
				$myposts = get_posts( $args );
				foreach ( $myposts as $post ) : setup_postdata( $post );
					$date = get_the_date( 'j/m/Y' );
					?>
                    <li>
                        <a href="<?php the_permalink(); ?>">
                            <span style="font-size: 85%;font-style: italic"><?php echo $date; ?></span><br/>
                            <span><?php echo ucfirst( strtolower( get_the_title() ) ); ?></span>
                        </a>
                    </li>
					<?php
				endforeach;
				wp_reset_postdata();
				?>
            </ul>
        </li>
    </ul>
	<?php
	// Includo prima il menu primario
	wp_nav_menu( array(
			'menu'            => 'menu1',
			'theme_location'  => 'menu1',
			'depth'           => 2,
			'container'       => '',
			'container_class' => '',
			'menu_class'      => 'slider-menu list-group',
			'fallback_cb'     => 'wp_slider_navwalker::fallback',
			'walker'          => new wp_slider_navwalker()
		)
	);

	// Includo poi il menu secondario
	wp_nav_menu( array(
			'menu'            => 'menu2',
			'theme_location'  => 'menu2',
			'depth'           => 2,
			'container'       => '',
			'container_class' => '',
			'menu_class'      => 'slider-menu list-group',
			'fallback_cb'     => 'wp_slider_navwalker::fallback',
			'walker'          => new wp_slider_navwalker()
		)
	);
	?>
    <div style="height:50px">&nbsp;</div>
</div> <!-- navigation -->