/**
function cax_pdf() {
    return "<p>[BANNER_CODE]</p>";
}
**/

(function() {
  tinymce.create("tinymce.plugins.cax_pdf", {init:function(ed, url) {
    ed.addButton("cax_pdf", {title:"Includi un PDF per la visualizzazione", onclick:function() {
      var textprompt = prompt("Inserisci l'indirizzo completo del PDF");
      if (textprompt) {
        tinyMCE.execInstanceCommand("mce_editor_0", "mceFocus");
        ed.execCommand("mceInsertContent", false, "[gpdf]" + textprompt + "[/gpdf]");
      }
    }, image:url + "/pdf-ico.png"});
  }});
  tinymce.PluginManager.add("cax_pdf", tinymce.plugins.cax_pdf);
})();

