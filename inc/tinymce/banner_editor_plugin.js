function cax_banner() {
    return "<p>[BANNER_CODE]</p>";
}

(function() {

    tinymce.create('tinymce.plugins.cax_banner', {

        init : function(ed, url){
            ed.addButton('cax_banner', {
                title : 'Includi il banner',
                onclick : function() {
                    ed.execCommand(
                        'mceInsertContent',
                        false,
                        cax_banner()
                        );
                },
                image: url + "/banner.png"
            });
        },

    });

    tinymce.PluginManager.add('cax_banner', tinymce.plugins.cax_banner);
    
})();
