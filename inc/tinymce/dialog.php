<?php
if (!defined('ABSPATH'))
    die("Can't load this file directly");

@header('Content-Type: ' . get_option('html_type') . '; charset=' . get_option('blog_charset'));
?>
<!doctype html>

<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Inserisci Gallerie e Video</title>
        <link rel='stylesheet' id='jquery-ui-css-css'  href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/flick/jquery-ui.css?ver=3.4.2' type='text/css' media='all' />
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/inc/tinymce/kube.css" />
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/select2.css" />

        <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
        <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.20/jquery-ui.min.js'></script>
        <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/select2.min.js'></script>
        <script language="javascript" type="text/javascript" src="<?php echo site_url(); ?>/wp-includes/js/tinymce/tiny_mce_popup.js"></script>

        <script type='text/javascript'>

        </script>
        <style type="text/css">
            .ui-widget .ui-widget {
                font-size: 1.1em;
            }
            .ui-corner-top {
                font-size: 90%;
            }
        </style>
    </head>
    <body>

        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">Contenuti nelle notizie e recensioni</a></li>
                <li><a href="#tabs-2">Gallery nelle Gallerie</a></li>
                <li><a href="#tabs-3">Video Webnews (Iframe)</a></li>
            </ul>
            <div id="tabs-1">
                <div id="wnlinktopost-form">
                    <fieldset style="padding: 4px 20px;">
                        <legend>Ricerca</legend><br>
                        <input type="text" id="wnlinktopost" name="columns" value="" style="width:500px"/><br />
                        <p>Digita almeno tre lettere per cercare. Digita <strong>123</strong> per l'elenco delle ultime pubblicazioni.</p>
                    </fieldset>
                    <fieldset style="padding: 4px 20px; margin-top: 10px;">
                        <legend>Contenuto da cercare</legend>
                        <div id="format" style="padding: 5px 0;">
                            <input type="checkbox" id="check1" value="post" /><label for="check1">Notizia</label>
                            <input type="checkbox" id="check8" value="speciale" /><label for="check8">Speciale</label>
                            <input type="checkbox" id="check2" value="recensione" /><label for="check2">Prodotti</label>
                            <input type="checkbox" id="check7" value="videogioco" /><label for="check7">Giochi</label>
                            <input type="checkbox" id="check3" value="galleria" checked="checked" /><label for="check3">Galleria</label>
                            <input type="checkbox" id="check4" value="video" /><label for="check4">Video</label>
                            <input type="checkbox" id="check5" value="timeline" /><label for="check5">Timeline</label>
                            <input type="checkbox" id="check6" value="tema" /><label for="check6">Tema</label>
                        </div>
                    </fieldset>
                    <p class="submit" style="padding: 15px 0;">
                        <input type="submit" id="wnlinktopost-submit-close" class="button-primary" value="Inserisci e chiudi" name="submit" />
                        <input type="submit" id="wnlinktopost-submit" class="button-primary" value="Inserisci" name="submit" />
                    </p>
                </div>
            </div>
            <div id="tabs-2">
                <div id="wngaltogal-form">
                    <fieldset style="padding: 4px 20px;">
                        <legend>Gallerie di Nextgen</legend><br>
                        <input type="text" id="wngaltogal" name="columns" value="" style="width:500px"/><br />
                        <p>Scegliere la Gallery creata con NetxGenGallery da includere nella Galleria.</p></td>
                    </fieldset>
                    <p class="submit" style="padding: 15px 0;">
                        <input type="submit" id="wngaltogal-submit-close" class="button-primary" value="Inserisci e chiud" name="submit" />
                        <input type="submit" id="wngaltogal-submit" class="button-primary" value="Inserisci" name="submit" />
                    </p>
                </div>
            </div>
            <div id="tabs-3">
                <div id="wnvideo-form">
                    <fieldset style="padding: 4px 20px;">
                        <legend>Video Webnews</legend><br>
                        <input type="text" id="wnvideo" name="columns" value="" style="width:500px"/><br />
                        <p>Selezionare il video di Webnews da "embeddare" all'interno di un articolo. Digita <strong>123</strong> per l'elenco delle ultime pubblicazioni.</p></td>
                    </fieldset>
                    <p class="submit" style="padding: 15px 0;">
                        <input type="submit" id="wnvideo-submit-close" class="button-primary" value="Inserisci e chiudi" name="submit" />
                        <input type="submit" id="wnvideo-submit" class="button-primary" value="Inserisci" name="submit" />
                    </p>
                </div>
            </div>
        </div>



        <script>
            var MyAjax = {"ajaxurl":"http:\/\/www.webnews.it\/wp-admin\/admin-ajax.php","search_nonce":"c0235fd10e"};

            jQuery(document).ready(function($) { 
                $( "#tabs" ).tabs();
                jQuery( "#check" ).button();
                jQuery( "#format" ).buttonset();
                  
                $(function() {
                    $( "input[type=submit]" )
                    .button()
                    .click(function( event ) {
                        event.preventDefault();
                    });
                });
                
                $("#wnlinktopost").select2({
                    placeholder: "Cerca un contenuto",
                    minimumInputLength: 3,
                    ajax: {
                        url: MyAjax.ajaxurl,
                        dataType: 'jsonp',
                        data: function (term, page) {
                            return {
                                action:"my_ajax_search_posts",  limit: 30,  mrp_post_type:fsc, mrp_res:3, q:term
                            };
                        },
                        results: function (data, page) { 
                            return {results: data};
                        }
                    },
                    formatResult: formatResult,
                    formatSelection: formatSelection
                });

                $("#wngaltogal").select2({
                    placeholder: "Cerca una gallery",
                    minimumInputLength: 3,
                    ajax: {
                        url: '<?php echo home_url('index.php'); ?>/',
                        dataType: 'json',
                        data: function (term, page) {
                            return {
                                method:'autocomplete',  type: 'gallery', format:'json', callback:'json', limit:50, term:term
                            };
                        },
                        results: function (data, page) { 
                            return {results: data};
                        }
                    },
                    formatResult: ResultGalToGal,
                    formatSelection: SelectionGalToGal
                });
                $("#wnvideo").select2({
                    placeholder: "Cerca un video",
                    allowClear: true,
                    minimumInputLength: 3,
                    ajax: {
                        url: MyAjax.ajaxurl,
                        dataType: 'jsonp',
                        data: function (term, page) {
                            return {
                                action:"my_ajax_search_video",  limit: 30, q:term
                            };
                        },
                        results: function (data, page) { 
                            return {results: data};
                        }
                    },
                    formatResult: formatResult,
                    formatSelection: formatSelection
                });
            });
		 
            function formatResult(item) { 
                var markup = item.title + ' (' + item.data + ' - ' + item.type + ')';
                return markup;
            }
            
            function ResultGalToGal(item) { 
                var markup = '(' + item.id + ') ' + item.value;
                return markup;
            }

            function formatSelection(item) { posttype = item.type; return item.title; };
            function SelectionGalToGal(item) { return item.value; };

            // handles the click event of the submit button
            $('#wngaltogal-submit').click(function(){
                var shortcode = '[nggallery';
                var value = jQuery("#wngaltogal").select2("val");
                shortcode += ' ' + 'id' + '=' + value + ' template=carousel';
                shortcode += ']';

                // inserts the shortcode into the active editor
                tinyMCE.activeEditor.execCommand('mceInsertContent', 0, shortcode);
            });
            
            $('#wngaltogal-submit-close').click(function(){
                var shortcode = '[nggallery';
                var value = jQuery("#wngaltogal").select2("val");
                shortcode += ' ' + 'id' + '=' + value + ' template=carousel';
                shortcode += ']';

                // inserts the shortcode into the active editor
                tinyMCE.activeEditor.execCommand('mceInsertContent', 0, shortcode);
			
                // closes Thickbox
                tinyMCEPopup.close();
            });
            
            // handles the click event of the submit button
            $('#wnlinktopost-submit-close').click(function(){
                
                if (posttype == 'galleria') {
                    var shortcode = '[wngallery';
                    var value = jQuery("#wnlinktopost").select2("val");
                    shortcode += ' ' + 'postid' + '=' + value;
                    shortcode += ']';
                } else if (posttype == 'video') {
                    var shortcode = '[nggvideo';
                    var value = jQuery("#wnlinktopost").select2("val");
                    shortcode += ' ' + 'id' + '=' + value;
                    shortcode += ']';
                } else if (posttype == 'videogioco' || posttype == 'recensione') {
                    var shortcode = '[nggcard';
                    var value = jQuery("#wnlinktopost").select2("val");
                    shortcode += ' ' + 'id' + '=' + value;
                    shortcode += ']';
                } else if (posttype == 'speciale') {
                    var shortcode = '[nggspeciale';
                    var value = jQuery("#wnlinktopost").select2("val");
                    shortcode += ' ' + 'id' + '=' + value;
                    shortcode += ']';
                } else {
                    var shortcode = '[ngglink';
                    var value = jQuery("#wnlinktopost").select2("val");
                    shortcode += ' ' + 'id' + '=' + value;
                    shortcode += ']';
                }
                // inserts the shortcode into the active editor
                tinyMCE.activeEditor.execCommand('mceInsertContent', 0, shortcode);
			
                // closes Thickbox
                tinyMCEPopup.close();
            });
            
            $('#wnlinktopost-submit').click(function(){
                
                if (posttype == 'galleria') {
                    var shortcode = '[wngallery';
                    var value = jQuery("#wnlinktopost").select2("val");
                    shortcode += ' ' + 'postid' + '=' + value;
                    shortcode += ']';
                } else if (posttype == 'video') {
                    var shortcode = '[nggvideo';
                    var value = jQuery("#wnlinktopost").select2("val");
                    shortcode += ' ' + 'id' + '=' + value;
                    shortcode += ']';
                } else if (posttype == 'videogioco' || posttype == 'recensione') {
                    var shortcode = '[nggcard';
                    var value = jQuery("#wnlinktopost").select2("val");
                    shortcode += ' ' + 'id' + '=' + value;
                    shortcode += ']';
                } else if (posttype == 'speciale') {
                    var shortcode = '[nggspeciale';
                    var value = jQuery("#wnlinktopost").select2("val");
                    shortcode += ' ' + 'id' + '=' + value;
                    shortcode += ']';
                } else {
                    var shortcode = '[ngglink';
                    var value = jQuery("#wnlinktopost").select2("val");
                    shortcode += ' ' + 'id' + '=' + value;
                    shortcode += ']';
                }
                // inserts the shortcode into the active editor
                tinyMCE.activeEditor.execCommand('mceInsertContent', 0, shortcode);

            });
            
            $('#wnvideo-submit-close').click(function(){
                
                if (posttype == 'Webnews') {
                    var shortcode = '[wnvideo]';
                    var value = jQuery("#wnvideo").select2("val");
                    shortcode += value;
                    shortcode += '[/wnvideo]';
                } else {
                    var shortcode = '[nggvideo';
                    var value = jQuery("#wnvideo").select2("val");
                    shortcode += ' ' + 'id' + '=' + value;
                    shortcode += ']';
                } 
                
                // inserts the shortcode into the active editor
                tinyMCE.activeEditor.execCommand('mceInsertContent', 0, shortcode);
			
                // closes Thickbox
                tinyMCEPopup.close();
            });
            
            $('#wnvideo-submit').click(function(){
                if (posttype == 'Webnews') {
                    var shortcode = '[wnvideo]';
                    var value = jQuery("#wnvideo").select2("val");
                    shortcode += value;
                    shortcode += '[/wnvideo]';
                } else {
                    var shortcode = '[nggvideo';
                    var value = jQuery("#wnvideo").select2("val");
                    shortcode += ' ' + 'id' + '=' + value;
                    shortcode += ']';
                } 

                // inserts the shortcode into the active editor
                tinyMCE.activeEditor.execCommand('mceInsertContent', 0, shortcode);
            });
            
            var fsc = $('#format input[type=checkbox]:checked').map(function(){return this.value}).get();
            $('#format input[type=checkbox]').change(function () {
                fsc = $('#format input[type=checkbox]:checked').map(function(){return this.value}).get();
            });
            var posttype = '';
        </script>
    </body>
</html>