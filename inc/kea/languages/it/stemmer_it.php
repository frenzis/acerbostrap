<?php
// $Id: italianstemmer.module,v 1.1 2009/11/08 12:00:53 Alex72RM Exp $

/**
 * @file
 * The implementation of the Italian Stemmer is free software,
 * and is written by Alessandro Bonelli (http://www.alessandrobonelli.it). Certain elements
 * were borrowed from the (broken) implementation by Jon Abernathy.
 *
 * It was modified by Alessandro Bonelli for PHP5 compatibility and Drupal integration.
 */

/**
 *  Subsequent PHP class is written by Roberto Mirizzi (roberto.mirizzi@gmail.com,  http://sisinflab.poliba.it/mirizzi/) in February 2007.
 *  It is the PHP5 implementation of Martin Porter's stemming algorithm for Italian language.
 *  This algorithm can be found at address: http://snowball.tartarus.org/algorithms/italian/stemmer.html.
 *  Use the code freely. I'm not responsible for any problems.
 *  All Italian characters are (originally) in latin1 (ISO-8859-1).
 *
 */

$vocali = array('a', 'e', 'i', 'o', 'u', 'à', 'è', 'ì', 'ò', 'ù');
$consonanti = array('b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z', 'I', 'U');
$accenti_acuti = array('á', 'é', 'í', 'ó', 'ú');
$accenti_gravi = array('à', 'è', 'ì', 'ò', 'ù');

$suffissi_step_0 = array('ci', 'gli', 'la', 'le', 'li', 'lo', 'mi', 'ne', 'si', 'ti', 'vi', 'sene', 'gliela', 'gliele', 'glieli', 'glielo', 'gliene', 'mela', 'mele', 'meli', 'melo', 'mene', 'tela', 'tele', 'teli', 'telo', 'tene', 'cela', 'cele', 'celi', 'celo', 'cene', 'vela', 'vele', 'veli', 'velo', 'vene');

$suffissi_step_1_a = array('anza', 'anze', 'ico', 'ici', 'ica', 'ice', 'iche', 'ichi', 'ismo', 'ismi', 'abile', 'abili', 'ibile', 'ibili', 'ista', 'iste', 'isti', 'istà', 'istè', 'istì', 'oso', 'osi', 'osa', 'ose', 'mente', 'atrice', 'atrici', 'ante', 'anti');
$suffissi_step_1_b = array('azione', 'azioni', 'atore', 'atori');
$suffissi_step_1_c = array('logia', 'logie');
$suffissi_step_1_d = array('uzione', 'uzioni', 'usione', 'usioni');
$suffissi_step_1_e = array('enza', 'enze');
$suffissi_step_1_f = array('amento', 'amenti', 'imento', 'imenti');
$suffissi_step_1_g = array('amente');
$suffissi_step_1_h = array('ità');
$suffissi_step_1_i = array('ivo', 'ivi', 'iva', 'ive');

$suffissi_step_2 = array('ammo', 'ando', 'ano', 'are', 'arono', 'asse', 'assero', 'assi', 'assimo', 'ata', 'ate', 'ati', 'ato', 'ava', 'avamo', 'avano', 'avate', 'avi', 'avo', 'emmo', 'enda', 'ende', 'endi', 'endo', 'erà', 'erai', 'eranno', 'ere', 'erebbe', 'erebbero', 'erei', 'eremmo', 'eremo', 'ereste', 'eresti', 'erete', 'erò', 'erono', 'essero', 'ete', 'eva', 'evamo', 'evano', 'evate', 'evi', 'evo', 'Yamo', 'iamo', 'immo', 'irà', 'irai', 'iranno', 'ire', 'irebbe', 'irebbero', 'irei', 'iremmo', 'iremo', 'ireste', 'iresti', 'irete', 'irò', 'irono', 'isca', 'iscano', 'isce', 'isci', 'isco', 'iscono', 'issero', 'ita', 'ite', 'iti', 'ito', 'iva', 'ivamo', 'ivano', 'ivate', 'ivi', 'ivo', 'ono', 'uta', 'ute', 'uti', 'uto', 'ar', 'ir');

$ante_suff_a = array('ando', 'endo');
$ante_suff_b = array('ar', 'er', 'ir');

usort($suffissi_step_0, create_function('$a, $b', 'return drupal_strlen($a)>drupal_strlen($b) ? -1 : 1;'));
usort($suffissi_step_1_a, create_function('$a, $b', 'return drupal_strlen($a)>drupal_strlen($b) ? -1 : 1;'));
usort($suffissi_step_2, create_function('$a, $b', 'return drupal_strlen($a)>drupal_strlen($b) ? -1 : 1;'));

/**
 * Strip numbers from text.
 */
function _unicode_caseflip($matches) {
    return $matches[0][0] . chr(ord($matches[0][1]) ^ 32);
}

function drupal_strtoupper($text) {
  global $multibyte;
  if ($multibyte == 'UNICODE_MULTIBYTE') {
    return mb_strtoupper($text);
  }
  else {
    // Use C-locale for ASCII-only uppercase
    $text = strtoupper($text);
    // Case flip Latin-1 accented letters
    $text = preg_replace_callback('/\xC3[\xA0-\xB6\xB8-\xBE]/', '_unicode_caseflip', $text);
    return $text;
  }
}


function drupal_strlen($text) {
    global $multibyte;
    return mb_strlen($text);
    if ($multibyte == 'UNICODE_MULTIBYTE') {
        return mb_strlen($text);
    } else {
        // Do not count UTF-8 continuation bytes.
        return strlen(preg_replace("/[\x80-\xBF]/", '', $text));
    }
}

function drupal_strtolower($text) {
    global $multibyte;
    return mb_strtolower($text);
    if ($multibyte == 'UNICODE_MULTIBYTE') {
        return mb_strtolower($text);
    } else {
        // Use C-locale for ASCII-only lowercase
        $text = strtolower($text);
        // Case flip Latin-1 accented letters
        $text = preg_replace_callback('/\xC3[\x80-\x96\x98-\x9E]/', '_unicode_caseflip', $text);
        return $text;
    }
}

function drupal_substr($text, $start, $length = NULL) {
    global $multibyte;
    return $length === NULL ? mb_substr($text, $start) : mb_substr($text, $start, $length);
    if ($multibyte == 'UNICODE_MULTIBYTE') {
        return $length === NULL ? mb_substr($text, $start) : mb_substr($text, $start, $length);
    } else {
        $strlen = drupal_strlen($text);
        // Find the starting byte offset
        $bytes = 0;
        if ($start > 0) {
            // Count all the continuation bytes from the start until we have found
            // $start characters
            $bytes = -1;
            $chars = -1;
            while ($bytes < $strlen && $chars < $start) {
                $bytes++;
                if (isset($text[$bytes])) {
                    $c = ord($text[$bytes]);
                    if ($c < 0x80 || $c >= 0xC0) {
                        $chars++;
                    }
                }
            }
        } else if ($start < 0) {
            // Count all the continuation bytes from the end until we have found
            // abs($start) characters
            $start = abs($start);
            $bytes = $strlen;
            $chars = 0;
            while ($bytes > 0 && $chars < $start) {
                $bytes--;
                $c = ord($text[$bytes]);
                if ($c < 0x80 || $c >= 0xC0) {
                    $chars++;
                }
            }
        }
        $istart = $bytes;

        // Find the ending byte offset
        if ($length === NULL) {
            $bytes = $strlen - 1;
        } else if ($length > 0) {
            // Count all the continuation bytes from the starting index until we have
            // found $length + 1 characters. Then backtrack one byte.
            $bytes = $istart;
            $chars = 0;
            while ($bytes < $strlen && $chars < $length) {
                $bytes++;
                if (isset($text[$bytes])) {
                    $c = ord($text[$bytes]);
                    if ($c < 0x80 || $c >= 0xC0) {
                        $chars++;
                    }
                }
            }
            $bytes--;
        } else if ($length < 0) {
            // Count all the continuation bytes from the end until we have found
            // abs($length) characters
            $length = abs($length);
            $bytes = $strlen - 1;
            $chars = 0;
            while ($bytes >= 0 && $chars < $length) {
                $c = ord($text[$bytes]);
                if ($c < 0x80 || $c >= 0xC0) {
                    $chars++;
                }
                $bytes--;
            }
        }
        $iend = $bytes;

        return substr($text, $istart, max(0, $iend - $istart + 1));
    }
}

function to_lower($str) {
    return drupal_strtolower($str);
}

function replace_acc_acuti($str) {
    global $accenti_acuti;
    global $accenti_gravi;
    return str_replace($accenti_acuti, $accenti_gravi, $str);
}

function put_u_after_q_to_upper($str) {
    return str_replace("qu", "qU", $str);
}

function i_u_between_vow_to_upper($str) {
    $pattern = '/([aeiouàèìòù])([iu])([aeiouàèìòù])/e';
    $replacement = "'$1'.drupal_strtoupper('$2').'$3'";
    return preg_replace($pattern, $replacement, $str);
}

function return_RV($str) {
    global $consonanti;
    global $vocali;
    /*
      If the second letter is a consonant,  RV is the region after the next following vowel,
      or if the first two letters are vowels,  RV is the region after the next consonant,  and otherwise
      (consonant-vowel case) RV is the region after the third letter. But RV is the end of the word if these positions cannot be found.
      example,
      m a c h o [ho]     o l i v a [va]     t r a b a j o [bajo]     á u r e o [eo] prezzo sprezzante
     */

    if (drupal_strlen($str) < 2) {
        return '';
    }

    if (in_array($str[1], $consonanti)) {

        $str = drupal_substr($str, 2);
        $str = strpbrk($str, implode($vocali));
        return drupal_substr($str, 1); //secondo me devo mettere 1
    } else if (in_array($str[0], $vocali) && in_array($str[1], $vocali)) {
        $str = strpbrk($str, implode($consonanti));
        return drupal_substr($str, 1);
    } else if (in_array($str[0], $consonanti) && in_array($str[1], $vocali)) {
        return drupal_substr($str, 3);
    }
}

function return_R1($str) {
    /*
      R1 is the region after the first non-vowel following a vowel,  or is the null region at the end of the word if there is no such non-vowel.
      example:
      beautiful [iful]  beauty [y]  beau [NULL]  animadversion [imadversion]  sprinkled [kled]  eucharist [harist]
     */
    global $vocali;
    global $consonanti;
    $pattern = '/[' . implode($vocali) . ']+' . '[' . implode($consonanti) . ']' . '(.*)/';
    preg_match($pattern, $str, $matches);

    return count($matches) >= 1 ? $matches[1] : '';
}

function return_R2($str) {
    /*
      R2 is the region after the first non-vowel following a vowel in R1,  or is the null region at the end of the word if there is no such non-vowel.
      example:
      beautiful [ul]  beauty [NULL]  beau [NULL]  animadversion [adversion]  sprinkled [NULL]  eucharist [ist]
     */
    global $vocali;
    global $consonanti;
    $R1 = return_R1($str);

    $pattern = '/[' . implode($vocali) . ']+' . '[' . implode($consonanti) . ']' . '(.*)/';
    preg_match($pattern, $R1, $matches);

    return count($matches) >= 1 ? $matches[1] : '';
}

function step_0($str) {
    global $suffissi_step_0;
    global $ante_suff_a;
    global $ante_suff_b;
    //Step 0: Attached pronoun
    //Always do steps 0

    $str_len = drupal_strlen($str);
    $rv = return_RV($str);
    $rv_len = drupal_strlen($rv);

    $pos = 0;
    foreach ($suffissi_step_0 as $suff) {
        if ($rv_len - drupal_strlen($suff) < 0) {
            continue;
        }
        $pos = strpos($rv, $suff, $rv_len - drupal_strlen($suff));
        if ($pos !== FALSE) {
            break;
        }
    }

    $ante_suff = drupal_substr($rv, 0, $pos);
    $ante_suff_len = drupal_strlen($ante_suff);

    foreach ($ante_suff_a as $ante_a) {
        if ($ante_suff_len - drupal_strlen($ante_a) < 0) {
            continue;
        }
        $pos_a = strpos($ante_suff, $ante_a, $ante_suff_len - drupal_strlen($ante_a));
        if ($pos_a !== FALSE) {
            return drupal_substr($str, 0, $pos + $str_len - $rv_len);
        }
    }

    foreach ($ante_suff_b as $ante_b) {
        if ($ante_suff_len - drupal_strlen($ante_b) < 0) {
            continue;
        }
        $pos_b = strpos($ante_suff, $ante_b, $ante_suff_len - drupal_strlen($ante_b));
        if ($pos_b !== FALSE) {
            return drupal_substr($str, 0, $pos + $str_len - $rv_len) . 'e';
        }
    }

    return $str;
}

function delete_suff($arr_suff, $str, $str_len, $where, $everywhere = FALSE) {
    if ($where === 'r2') {
        $r = return_R2($str);
    } else if ($where === 'rv') {
        $r = return_RV($str);
    } else if ($where === 'r1') {
        $r = return_R1($str);
    }

    $r_len = drupal_strlen($r);

    if ($everywhere) {
        foreach ($arr_suff as $suff) {
            if ($str_len - drupal_strlen($suff) < 0) {
                continue;
            }
            $pos = strpos($str, $suff, $str_len - drupal_strlen($suff));
            if ($pos !== FALSE) {
                $pattern = '/' . $suff . '$/';
                $ret_str = preg_match($pattern, $r) ? drupal_substr($str, 0, $pos) : '';
                if ($ret_str !== '') {
                    return $ret_str;
                }
                break;
            }
        }
    } else {
        foreach ($arr_suff as $suff) {
            if ($r_len - drupal_strlen($suff) < 0) {
                continue;
            }
            $pos = strpos($r, $suff, $r_len - drupal_strlen($suff));
            if ($pos !== FALSE) {
                return drupal_substr($str, 0, $pos + $str_len - $r_len);
            }
        }
    }
}

function step_1($str) {
    global $suffissi_step_1_g;
    global $suffissi_step_1_a;
    global $suffissi_step_1_b;
    global $suffissi_step_1_c;
    global $suffissi_step_1_d, $suffissi_step_1_e, $suffissi_step_1_f, $suffissi_step_1_g, $suffissi_step_1_h, $suffissi_step_1_i;
    //Step 1: Standard suffix removal
    //Always do steps 1
    $str_len = drupal_strlen($str);

    //delete if in R1, if preceded by 'iv',  delete if in R2 (and if further preceded by 'at',  delete if in R2),  otherwise,  if preceded by 'os',  'ic' or 'abil',  delete if in R2
    if (count($ret_str = delete_suff($suffissi_step_1_g, $str, $str_len, 'r1'))) {
        if (count($ret_str1 = delete_suff(array('iv'), $ret_str, drupal_strlen($ret_str), 'r2'))) {
            if (count($ret_str2 = delete_suff(array('at'), $ret_str1, drupal_strlen($ret_str1), 'r2'))) {
                return $ret_str2;
            } else {
                return $ret_str1;
            }
        } else if (count($ret_str1 = delete_suff(array('os', 'ic', 'abil'), $ret_str, drupal_strlen($ret_str), 'r2'))) {
            return $ret_str1;
        } else {
            return $ret_str;
        }
    }
    //delete if in R2
    if (count($ret_str = delete_suff($suffissi_step_1_a, $str, $str_len, 'r2', TRUE))) {
        return $ret_str;
    }
    //delete if in R2,    if preceded by 'ic',  delete if in R2
    if (count($ret_str = delete_suff($suffissi_step_1_b, $str, $str_len, 'r2'))) {
        if (count($ret_str1 = delete_suff(array('ic'), $ret_str, drupal_strlen($ret_str), 'r2'))) {
            return $ret_str1;
        } else {
            return $ret_str;
        }
    }
    //replace with 'log' if in R2
    if (count($ret_str = delete_suff($suffissi_step_1_c, $str, $str_len, 'r2'))) {
        return $ret_str . 'log';
    }
    //replace with 'u' if in R2
    if (count($ret_str = delete_suff($suffissi_step_1_d, $str, $str_len, 'r2'))) {
        return $ret_str . 'u';
    }
    //replace with 'ente' if in R2
    if (count($ret_str = delete_suff($suffissi_step_1_e, $str, $str_len, 'r2'))) {
        return $ret_str . 'ente';
    }
    //delete if in RV
    if (count($ret_str = delete_suff($suffissi_step_1_f, $str, $str_len, 'rv'))) {
        return $ret_str;
    }
    //delete if in R2, if preceded by 'abil', 'ic' or 'iv', delete if in R2
    if (count($ret_str = delete_suff($suffissi_step_1_h, $str, $str_len, 'r2'))) {
        if (count($ret_str1 = delete_suff(array('abil', 'ic', 'iv'), $ret_str, drupal_strlen($ret_str), 'r2'))) {
            return $ret_str1;
        } else {
            return $ret_str;
        }
    }
    //delete if in R2, if preceded by 'at', delete if in R2 (and if further preceded by 'ic',  delete if in R2)
    if (count($ret_str = delete_suff($suffissi_step_1_i, $str, $str_len, 'r2'))) {
        if (count($ret_str1 = delete_suff(array('at'), $ret_str, drupal_strlen($ret_str), 'r2'))) {
            if (count($ret_str2 = delete_suff(array('ic'), $ret_str1, drupal_strlen($ret_str1), 'r2'))) {
                return $ret_str2;
            } else {
                return $ret_str1;
            }
        } else {
            return $ret_str;
        }
    }

    return $str;
}

function step_2($str, $str_step_1) {
    //Step 2: Verb suffixes
    //Do step 2 if no ending was removed by step 1
    global $suffissi_step_2;

    if ($str != $str_step_1) {
        return $str_step_1;
    }

    $str_len = drupal_strlen($str);

    if (count($ret_str = delete_suff($suffissi_step_2, $str, $str_len, 'rv'))) {
        return $ret_str;
    }

    return $str;
}

function step_3a($str) {
    //Step 3a: Delete a final 'a',  'e',  'i',  'o', ' à',  'è',  'ì' or 'ò' if it is in RV,  and a preceding 'i' if it is in RV ('crocchi' -> 'crocch',  'crocchio' -> 'crocch')
    //Always do steps 3a

    $vocale_finale = array('a', 'e', 'i', 'o', 'à', 'è', 'ì', 'ò');

    $str_len = drupal_strlen($str);

    if (count($ret_str = delete_suff($vocale_finale, $str, $str_len, 'rv'))) {
        if (count($ret_str1 = delete_suff(array('i'), $ret_str, drupal_strlen($ret_str), 'rv'))) {
            return $ret_str1;
        } else {
            return $ret_str;
        }
    }

    return $str;
}

function step_3b($str) {
    //Step 3b: Replace final 'ch' (or 'gh') with 'c' (or 'g') if in 'RV' ('crocch' -> 'crocc')
    //Always do steps 3b

    $rv = return_RV($str);

    $pattern = '/([cg])h$/';
    $replacement = '${1}';
    return drupal_substr($str, 0, drupal_strlen($str) - drupal_strlen($rv)) . preg_replace($pattern, $replacement, $rv);
}

function step_4($str) {
    //Step 4: Finally,  turn I and U back into lower case
    return drupal_strtolower($str);
}

function stemmer_stem_it($str) {
    global $suffissi_step_0;
    global $suffissi_step_1_a;
    global $suffissi_step_2;

    $str = trim($str);
    $str = to_lower($str);
    $str = replace_acc_acuti($str);
    $str = put_u_after_q_to_upper($str);
    $str = i_u_between_vow_to_upper($str);
    $step0 = step_0($str);
    $step1 = step_1($step0);
    $step2 = step_2($step0, $step1);
    $step3a = step_3a($step2);
    $step3b = step_3b($step3a);
    $step4 = step_4($step3b);

    return $step4;
}


function stemit($str) {
    $str = trim($str);
    $str = to_lower($str);
    $str = replace_acc_acuti($str);
    $str = put_u_after_q_to_upper($str);
    $str = i_u_between_vow_to_upper($str);
    $step0 = step_0($str);
    $step1 = step_1($step0);
    $step2 = step_2($step0, $step1);
    $step3a = step_3a($step2);
    $step3b = step_3b($step3a);
    $step4 = step_4($step3b);

    return $step4;
}