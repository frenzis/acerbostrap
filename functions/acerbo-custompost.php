<?php

/**
 * POST TYPE
 */
add_action( 'init', 'acerbo_create_post_type' );

/**
 * Crea le capabilities per i post type rispettando singolare e plurale
 *
 * @param string $singular Il suffisso della capability al singolare
 * @param string $plural Il suffisso della capability al singolare
 *
 * @return array
 */
function compile_post_type_capabilities( $singular = 'post', $plural = 'posts' ) {
	return [
		'edit_post'              => "edit_$singular",
		'read_post'              => "read_$singular",
		'delete_post'            => "delete_$singular",
		'edit_posts'             => "edit_$plural",
		'edit_others_posts'      => "edit_others_$plural",
		'publish_posts'          => "publish_$plural",
		'read_private_posts'     => "read_private_$plural",
		'read'                   => "read",
		'delete_posts'           => "delete_$plural",
		'delete_private_posts'   => "delete_private_$plural",
		'delete_published_posts' => "delete_published_$plural",
		'delete_others_posts'    => "delete_others_$plural",
		'edit_private_posts'     => "edit_private_$plural",
		'edit_published_posts'   => "edit_published_$plural",
		'create_posts'           => "edit_$plural",
	];
}

/**
 * Aggiunge ai ruoli di default le capabilities dei post type
 */
function recreateCapabilities() {
	$roleAdmin          = get_role( 'administrator' );
	$roleEditor         = get_role( 'editor' );
	$capabilitiesArrays = array(
		compile_post_type_capabilities( 'appuntamento', 'appuntamenti' ),
		compile_post_type_capabilities( 'avviso', 'avvisi' ),
		compile_post_type_capabilities( 'circolare', 'circolari' ),
		compile_post_type_capabilities( 'contributo', 'contributi' )
	);
	foreach ( $capabilitiesArrays as $capabilitiesArray ) {
		foreach ( $capabilitiesArray as $capability ) {
			$roleAdmin->add_cap( $capability );
			$roleEditor->add_cap( $capability );
		}
	}
}

add_action( 'switch_theme', 'recreateCapabilities' );
add_action( 'after_switch_theme', 'recreateCapabilities' );

function acerbo_create_post_type() {
	register_post_type( 'contributi', array(
			'labels'        => array(
				'name'          => __( 'Contributi docenti' ),
				'singular_name' => __( 'Contributo' ),
				'add_new'       => __( 'Nuovo contributo' ),
				'add_new_item'  => __( 'Aggiungi nuovo contributo' ),
			),
			'public'        => true,
			'has_archive'   => true,
			'menu_position' => 5,
			'supports'      => array( 'title', 'editor', 'excerpt', 'revisions', 'thumbnail' ),
			'taxonomies'    => array( 'tag_docenti' ),
			'capabilities'  => compile_post_type_capabilities( 'contributo', 'contributi' )
		)
	);


	register_post_type( 'circolare', array(
			'labels'        => array(
				'name'          => __( 'Circolari' ),
				'singular_name' => __( 'Circolare' ),
				'add_new'       => __( 'Nuova circolare' ),
				'add_new_item'  => __( 'Aggiungi nuova circolare' ),
			),
			'public'        => true,
			'has_archive'   => true,
			'menu_position' => 5,
			'supports'      => array( 'title', 'editor', 'excerpt', 'revisions', 'thumbnail' ),
			'taxonomies'    => array( 'post_tag', 'category', 'tema' ),
			'capabilities'  => compile_post_type_capabilities( 'circolare', 'circolari' )
		)
	);

	register_post_type( 'appuntamento', array(
			'labels'        => array(
				'name'          => __( 'Appuntamenti' ),
				'singular_name' => __( 'Appuntamento' ),
				'add_new'       => __( 'Nuovo appuntamento' ),
				'add_new_item'  => __( 'Aggiungi nuovo appuntamento' ),
			),
			'public'        => true,
			'has_archive'   => true,
			'menu_position' => 7,
			'supports'      => array( 'title', 'editor', 'excerpt', 'revisions', 'thumbnail' ),
			'taxonomies'    => array( 'post_tag', 'category', 'tema' ),
			'capabilities'  => compile_post_type_capabilities( 'appuntamento', 'appuntamenti' )
		)
	);

	register_post_type( 'avviso', array(
			'labels'        => array(
				'name'          => __( 'Avvisi' ),
				'singular_name' => __( 'Avviso' ),
				'add_new'       => __( 'Nuovo avviso' ),
				'add_new_item'  => __( 'Aggiungi nuovo avviso' ),
			),
			'public'        => true,
			'has_archive'   => true,
			'menu_position' => 6,
			'supports'      => array( 'title', 'editor', 'excerpt', 'revisions', 'thumbnail' ),
			'taxonomies'    => array( 'post_tag', 'category', 'tema' ),
			'capabilities'  => compile_post_type_capabilities( 'avviso', 'avvisi' )
		)
	);
}


/**
 * TASSONOMIE
 */
add_action( 'init', 'create_acerbo_taxonomies', 0 );

function create_acerbo_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Tema', 'taxonomy general name' ),
		'singular_name'     => _x( 'Tema', 'taxonomy singular name' ),
		'search_items'      => __( 'Cerca temi' ),
		'all_items'         => __( 'Tutti temi' ),
		'parent_item'       => __( 'Tema padre' ),
		'parent_item_colon' => __( 'Tema padre:' ),
		'edit_item'         => __( 'Modifica tema' ),
		'update_item'       => __( 'Aggiorna tema' ),
		'add_new_item'      => __( 'Aggiungi nuovo tema' ),
		'new_item_name'     => __( 'Nuovo tema' ),
		'menu_name'         => __( 'Temi' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'public'            => true,
		'rewrite'           => array( 'slug' => 'tema' ),
	);

	register_taxonomy( 'tema', array( 'page', 'post', 'circolare', 'avviso', 'appuntamento' ), $args );
}
